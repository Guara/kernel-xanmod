#!/bin/bash

if [ -z ${logrunner_LOGNAME} ]; then
    export logrunner_LOGNAME="${1}+$(date +%s).log"
    echo "logrunner [${0}] INFO: Environment variable \`logrunner_LOGNAME' not set. Setting log file name to ${logrunner_LOGNAME}"
fi

echo "logrunner [${0}] INFO: Logs are saved to ${PWD}/${logrunner_LOGNAME}"
${@} 2>&1 | tee -a ${logrunner_LOGNAME}

exit ${PIPESTATUS[0]}
