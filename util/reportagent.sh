#!/bin/bash
fmt_TITLE_MESG=$(jq -rn --arg content "[${CI_PIPELINE_IID}: reportagent] ${1}" '$content|@uri')

DESC_MESG=$(cat <<EOF
Here's the last 16 lines of the log file ${reportagent_LOGNAME}:
---
$(tail -n 16 ${reportagent_LOGNAME})
---
EOF
)

fmt_DESC_MESG=$(jq -rn --arg content "${tmp_DESC_MESG}" '$content|@uri')

curl --request POST --header "PRIVATE-TOKEN: $REPORTAGENT_API_SECRET" "https://gitlab.freedesktop.org/api/v4/projects/${CI_PROJECT_ID}/issues?title=${fmt_TITLE_MESG}&labels=reportagent&description=${fmt_DESC_MESG}"
